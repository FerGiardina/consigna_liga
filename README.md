La Liga
=======

Dominio del problema
---------------------

De cara a tener una competencia justa, pareja y transparente La Liga Argentina de Futbol ha decidido implementar un conjunto de regulaciones referidas a la conformación de los equipos y la contratación de jugadores.

En primer lugar la liga tiene una limitación estatutaria que establece que el monto total del mercado es de $ 10.000.000. Esto implica que la sumatoria de la inversión de los 10 equipos que participan de la liga no puede superar ese monto.

Cada equipo tiene un presupuesto y una plantilla de hasta 32 jugadores. Se define para cada equipo dos métricas de cara a poder comparar la potencialidad de cada equipo: Poder Ofensivo y Poder Defensivo.

Cada jugador tiene un valor de mercado, una nacionalidad, posición donde juega (delantero, mediocampista, defensor o arquero) y una potencialidad (número entre 0 y 5). 
A partir de esto se puede calcular el poder ofensivo (potencialidad * 1) y el poder defensivo (potencialidad * -1)


Las regulaciones que pretenden implementarse en una primera etapa son:

* Cada equipo debe tener 2 arqueros
* La suma de los valores de los jugadores de un equipo no puede exceder el presupuesto del mismo
* Cada equipo puede tener hasta 3 jugadores extranjeros
* El poder ofensivo de un equipo se calcula como la cantidad de jugadores delanteros + 0.5 por la cantidad de mediocampistas + sumatoria del poder ofensivo del jugador
* El poder defensivo de un equipo se calcula como la cantidad de jugadores defensore + 0.5 por la cantidad de mediocampistas + cantidad de arqueros + sumatoria del (-1 * poder defensivo del jugador)
* La contactación de extrajeros implica un pago de un impuesto igual a 0.3 por el valor de mercado del jugador
* Los fichajes no pueden realizarse los fines de semana (sabado y domingo)


Funcionalidades a desarrollar
------------------------------


### Registro de equipo

```
Request
POST /equipos 
{
    "nombre": "san miguel",
    "presupuesto": 10000,
}

Response
201
{
    "id": 1,
    "nombre": "san miguel",
    "presupuesto": 10000,
}
```


### Lista de equipos

```
Request
GET /equipos

Response
200
[
    {
    "id": 1,
    "nombre": "san miguel",
    "presupuesto": 10000,
    "presupuesto_ejecutado": 10000,
    "estado": "COMPLETO",
    },
    {
    "id": 2,
    "nombre": "atu",
    "presupuesto": 34000,
    "estado": "INCOMPLETO",
    }
]


```

### Consulta de equipo

Debo poder consultar cada equipo para saber tu presupuesto utilizado, su plantilla, su poder ofensivo y defensivo

```
Request
GET /equipos/{id_equipo}

Response
200
{
    "id": 2,
    "nombre": "atu",
    "presupuesto": 34000,
    "presupuesto_ejecutado": 34000,
    "estado": "COMPLETO",
    "jugadores": [ {....}]
}
```



### Reset
Esta funcionalidad vuelve el sistema a cero y es usadada solo a fines de prueba.
```
Request
POST /reset 

Response
200
{ "resultado": "ok"}
```
