#language: es
Característica: Ficha de jugador

  Antecedentes:
    Dado que existe el equipo "River" con presupuesto 100

  @wip
  Escenario: f1 - Ficha exitosa
    Dado el jugador "Juan Perez" de nacionalidad "argentino"
    Y que es posicion "mediocampista"
    Y su valor de mercado es 50
    Y su potencialidad es 1
  	Cuando se ficha al jugador 
    Entonces obtengo un numero de fichaje

  @wip
  Escenario: f2 - Ficha fallida por exceso de extranjeros
    Dado que ficho al jugador "Juan Perez" de nacionalidad "chilena"
    Y que ficho al jugador "Juan Chilo" de nacionalidad "chilena"
    Y que ficho al jugador "Juan Chilote" de nacionalidad "chilena"
  	Cuando que ficho al jugador "Juan Chilote" de nacionalidad "armenia"
    Entonces obtengo un error de fichaje

  @wip
  Escenario: f3 - Ficha fallida por exceso de arqueros
    Dado que ficho al arquero "Juan Perez"
    Y que ficho al arquero "Pipo Perez"
  	Cuando ficho al arquero "Pipo Popo"
    Entonces obtengo un error de fichaje  

  @wip
  Escenario: f4 - Ficha fallida por exceso de presupuesto
    Dado que ficho al jugador "Juan Chilo" con valor  50
    Cuando ficho al jugador "Jose Lopez" con  valor 60
    Entonces obtengo un error de fichaje

  Escenario: f5 - Ficha fallida por dia sabado
    Dado que es sabado "2021-10-02"
    Cuando ficho al jugador "Juan Chilo"
    Entonces obtengo un error por fin de semana
